PROJECT_NAME = index
# Run the publishing functions from given .el file on given file path.
# Make sure to open emacs server/GUI in project directory.
EMACS = emacsclient --suppress-output --eval
# Directories:
CONFIG = config
DOCS = docs
SOURCE = src
# Specific directories for commands to run after publishing.
README = README.md
README_DOC = $(DOCS)/metadata.md
METADATA = src/data.js
# Get the root of the current Emacs server (ideally the project root).
SERVER_DIR = (with-current-buffer (current-buffer) default-directory)
DONE = @echo "\033[1m-> DONE\x1b[0m"

#----------------
# MAIN COMMANDS
#----------------
all: title publish $(README)

clean: title publish-force $(README)

title:
	@echo "\033[1m-------------------------\n Processing: $(PROJECT_NAME)\n-------------------------\x1b[0m"

.PHONY: all clean
#---------------------------
#	EMACS-HANDLED TARGETS
#---------------------------
publish: $(SOURCE)
	$(info    Publishing $<...)
	@$(EMACS) '(load-file (concat $(SERVER_DIR) "$(CONFIG)/$@.el"))'
	$(DONE)

publish-force: $(SOURCE)
	$(info    Publishing (Forced) $<...)
	@$(EMACS) '(load-file (concat $(SERVER_DIR) "$(CONFIG)/$@.el"))'
	$(DONE)
#------------------
#	OTHER TARGETS
#------------------
$(README): $(README_DOC)
	$(info    Building $@ from $<...)
	@cp $< $@
	$(DONE)

$(METADATA):
	python src/metadata.py -i resources -o src
	cp $(METADATA) public/data.js

metadata: $(METADATA)

rm-metadata: 
	rm $(METADATA)

metadata-clean: rm-metadata metadata
# Introduction

This is documentation on generating an index from html pages. The result is [here.](https://albertovaldez5.gitlab.io/data-analytics-notes/)

The following Python script will scrape all headers from html pages in a given directory and store the results as a Javascript array.

```shell
python --version
```

    Python 3.7.13


## Imports

We will start with our data structures, which is a `dataclass` to store the information and have a method or two. Then we need `json` to jsonify our dictionaries and dump them as strings.

```python
from dataclasses import dataclass
import json
```

Then for scraping the data we will use `Selenium` and `Beautiful Soup`. We could just use the later but we may want to interact with the DOM after Javascript processing for scraping other data later on, so we will include `Selenium` from the start.

```python
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver import Chrome
from bs4 import BeautifulSoup as Soup
```

And last but not least, we will use `argparse` and a few other libraries to make our lives easier. Note that we are using `asyncio` from the get go as we expect to be scraping about 20 pages each time we run the script. This complicates things a little bit but we are going to keep the `async` functions to a minimal. It&rsquo;s also for fun.

One last thing, we are going to import a few types from `typing` to make sure our function calls and data types are correct as well as regular expressions because they are always useful.

```python
from argparse import ArgumentParser, Namespace
from typing import AsyncGenerator, Callable, Generator
from pathlib import Path
import asyncio
import re
```


## Classes and Constants

We want just one constant which will be a list of banned html files that won&rsquo;t be included in the result. Then we will have one `dataclass` to carry our information and centralize all the calls to scraping functions in a same method.

The `scrape` method will simply run any functions pass to it using the html attribute of the class as the only argument for the function. This means all functions for scraping should have one job only which is to travese the given `Soup` and return the resulting data.

```python
BANS = ["scraper"] # banned filenames

@dataclass
class Metadata:
    """Holds data to be scraped from an html file."""

    name: str
    path: Path
    html: Soup

    def scrape(self, *args: Callable[[Soup], Generator]) -> dict:
        """Apply all given scraping functions to the metadata's html attribute.

        Returns:
            (dict): {name, path, data}
        """
        return {
            "name": f"{self.name}",
            "path": f"{self.path}",
            "data": [data for func in args for data in func(self.html)],
        }

    def __repr__(self) -> str:
        return (
            f"-----Metadata-----\n"
            f"name: {self.name}\n"
            f"path: {self.path}\n"
            f"------------------"
        )

```


## Inputs and Outputs

We need a few utility functions. The first one will get us a headless browser and the second one will be the function we use to write to our output file.

```python
def get_browser() -> Chrome:
    """Start a headless Chrome browser."""
    options = Options()
    options.headless = True
    service = Service(ChromeDriverManager().install())
    return Chrome(service=service, options=options)


def write_to_js(path: Path, data: str, mode: str, name: str = "data.js"):
    """Write string to data.js in given path."""
    with open(path / name, mode) as file:
        file.write(data)

```


## Scraping Functions

We have two types of functions here, one is the main loop that simply loads the html into the `Metadata` object. The second one is the type of function that is called from the `Metadata`&rsquo;s `scrape` method. Note that the main html loop should also check for any banned names before loading them as `Soup` objects.

There are a few side effects in the main loop, as we are using a global variable and we are calling a method for the browser object that quits its process once we are done with the loop. We will try to keep all uncertainty in this function and make sure the rest are &ldquo;pure&rdquo; so we don&rsquo;t spread the decision making across many steps.

```python
async def get_html(browser: Chrome, path: Path, glob: str) -> AsyncGenerator[Metadata, None]:
    """Visit all html files in given path and get their name and html content.

    Args:
        browser (Chrome): Driver to use.
        path (Path): Path where all html files are located.
        glob: (str): Complete glob pattern to use.

    Yields:
        AsyncGenerator[Metadata, None, None]: Metadata object.
    """
    for file in path.resolve().glob(glob):
        browser.get(f"file://{path / file}")
        if browser.page_source is not None and file.stem not in BANS:
            filepath = file.relative_to(file.parent.parent.parent.parent)
            yield Metadata(file.stem, filepath, Soup(browser.page_source, "html.parser"))
    browser.quit()


def get_headers(html: Soup) -> Generator[dict, None, None]:
    """Find all h2 and h3 headers in given Soup.

    Args:
        html (Soup): parsed html.

    Yields:
        Generator[dict, None, None]: dictionary of id, text.
    """
    for h in html.find_all(re.compile('^h[2-3]$')):
        if h.get("id") is not None:
            yield {
                "id": h.get("id"),
                "text": h.text,
                "name": h.name
            }

```


## Main Function

Now that we have all the pieces, we can put them together in a simple `async for loop` that will make use of our `async generator` to load the files and write the output one by one. We chose to write all the results to the same file and format our data as a `json` string, which is not ideal. Some alternatives are storing individual `json` files or quering a database which could be better for larger scale projects.

```python
async def main(argv: Namespace):
    """Execute all scraping tasks and store results in files.

    Args:
        argv (Namespace): -h

    Raises:
        ValueError: If input directory doesn't exist.
    """
    input_path = Path(argv.input)
    output_path = Path(argv.output)
    if not input_path.is_dir():
        raise ValueError(f"Directory {input_path} doesn't exist.")
    if not output_path.is_dir():
        Path.mkdir(output_path)
    # Scrape
    write_to_js(output_path, "let headerData = [\n", "w")
    async for metadata in get_html(get_browser(), input_path, "**/build/*.html"):
        print(metadata)
        result = json.dumps(metadata.scrape(get_headers), indent=4)
        write_to_js(output_path, f"{result},\n", "a")
    write_to_js(output_path, "\n]", "a")

```


## The Program

Now that our main function is done, we can create our argument parser in the `__main__` process. We will set a few defaults because we know we are running the script for this particular case but we provide details on how to call the program and what arguments to give it.

```python
if __name__ == "__main__":
    default_input = "./resources"
    default_output = "./public"
    args = ArgumentParser(
        prog="Metadata scraper",
        usage=f"python config/metadata.py -i {default_input} -o {default_output}",
        description="Scrape id, text metadata from HTML files.",
    )
    args.add_argument(
        "-i",
        "--input",
        metavar="path",
        default=f"{default_input}",
        help=f"Directory with the files to scrape. Defaults to {default_input}.",
    )
    args.add_argument(
        "-o",
        "--output",
        metavar="path",
        default=f"{default_output}",
        help=f"Directory for files output. Defaults to {default_output}",
    )
    asyncio.run(main(args.parse_args()))

```


## Final Script

This is the complete script with all the parts we previously described.

```python
from dataclasses import dataclass
import json
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver import Chrome
from bs4 import BeautifulSoup as Soup
from argparse import ArgumentParser, Namespace
from typing import AsyncGenerator, Callable, Generator
from pathlib import Path
import asyncio
import re

BANS = ["scraper"] # banned filenames

@dataclass
class Metadata:
    """Holds data to be scraped from an html file."""

    name: str
    path: Path
    html: Soup

    def scrape(self, *args: Callable[[Soup], Generator]) -> dict:
        """Apply all given scraping functions to the metadata's html attribute.

        Returns:
            (dict): {name, path, data}
        """
        return {
            "name": f"{self.name}",
            "path": f"{self.path}",
            "data": [data for func in args for data in func(self.html)],
        }

    def __repr__(self) -> str:
        return (
            f"-----Metadata-----\n"
            f"name: {self.name}\n"
            f"path: {self.path}\n"
            f"------------------"
        )

async def main(argv: Namespace):
    """Execute all scraping tasks and store results in files.

    Args:
        argv (Namespace): -h

    Raises:
        ValueError: If input directory doesn't exist.
    """
    input_path = Path(argv.input)
    output_path = Path(argv.output)
    if not input_path.is_dir():
        raise ValueError(f"Directory {input_path} doesn't exist.")
    if not output_path.is_dir():
        Path.mkdir(output_path)
    # Scrape
    write_to_js(output_path, "let headerData = [\n", "w")
    async for metadata in get_html(get_browser(), input_path, "**/build/*.html"):
        print(metadata)
        result = json.dumps(metadata.scrape(get_headers), indent=4)
        write_to_js(output_path, f"{result},\n", "a")
    write_to_js(output_path, "\n]", "a")

async def get_html(browser: Chrome, path: Path, glob: str) -> AsyncGenerator[Metadata, None]:
    """Visit all html files in given path and get their name and html content.

    Args:
        browser (Chrome): Driver to use.
        path (Path): Path where all html files are located.
        glob: (str): Complete glob pattern to use.

    Yields:
        AsyncGenerator[Metadata, None, None]: Metadata object.
    """
    for file in path.resolve().glob(glob):
        browser.get(f"file://{path / file}")
        if browser.page_source is not None and file.stem not in BANS:
            filepath = file.relative_to(file.parent.parent.parent.parent)
            yield Metadata(file.stem, filepath, Soup(browser.page_source, "html.parser"))
    browser.quit()


def get_headers(html: Soup) -> Generator[dict, None, None]:
    """Find all h2 and h3 headers in given Soup.

    Args:
        html (Soup): parsed html.

    Yields:
        Generator[dict, None, None]: dictionary of id, text.
    """
    for h in html.find_all(re.compile('^h[2-3]$')):
        if h.get("id") is not None:
            yield {
                "id": h.get("id"),
                "text": h.text,
                "name": h.name
            }

def get_browser() -> Chrome:
    """Start a headless Chrome browser."""
    options = Options()
    options.headless = True
    service = Service(ChromeDriverManager().install())
    return Chrome(service=service, options=options)


def write_to_js(path: Path, data: str, mode: str, name: str = "data.js"):
    """Write string to data.js in given path."""
    with open(path / name, mode) as file:
        file.write(data)

if __name__ == "__main__":
    default_input = "./resources"
    default_output = "./public"
    args = ArgumentParser(
        prog="Metadata scraper",
        usage=f"python config/metadata.py -i {default_input} -o {default_output}",
        description="Scrape id, text metadata from HTML files.",
    )
    args.add_argument(
        "-i",
        "--input",
        metavar="path",
        default=f"{default_input}",
        help=f"Directory with the files to scrape. Defaults to {default_input}.",
    )
    args.add_argument(
        "-o",
        "--output",
        metavar="path",
        default=f"{default_output}",
        help=f"Directory for files output. Defaults to {default_output}",
    )
    asyncio.run(main(args.parse_args()))

```

We can run our script now.

```shell
python metadata.py -i "../resources" -o "../public"
```

    -----Metadata-----
    name: etl
    path: week-08/public/build/etl.html
    ------------------
    -----Metadata-----
    name: sql
    path: week-07/public/build/sql.html
    ------------------
    -----Metadata-----
    name: sqlalchemy
    path: week-09/public/build/sqlalchemy.html
    ------------------
    -----Metadata-----
    name: plotly
    path: week-12/public/build/plotly.html
    ------------------
    -----Metadata-----
    name: web
    path: week-10/public/build/web.html
    ------------------
    -----Metadata-----
    name: javascript
    path: week-11/public/build/javascript.html
    ------------------


# Displaying the Index

Now that the data is stored in a file, we will load it with html and Javascript.


## Data

We want to display our data from an array of objects that looks like this:

```js
let headerData = [
{
    "name": "etl",
    "path": "week-08/public/build/etl.html",
    "data": [
        {
            "id": "etl-process",
            "text": "1. ETL process",
            "name": "h2"
        },
```

So we are going to create a script that uses `headerData` and constructs html elements from its values. In this case we want a list of headers with a hyperlink to a url that matches both the file directory and the id of each header, so we can effectively create an index.

We also want to make sure we sort it before displaying it so we are going to create a function to do help us get their index from a string and then just call `sort` on the array. We have a few constants and &ldquo;magic literals&rdquo; specific to this project, but once again it&rsquo;s not a big deal because the script itself is project specific.

Note that we are adding the elements to both the `content` part of the webpage as well as the `table-of-contents` part of it.


## HTML

This is the html snippet we are going to add to our document.

```html
<ul id="headerList">
</ul>
<script src="data.js"></script>
<script src="index.js"></script>
```


## Script

We can write our script now. Let&rsquo;s start with loading the data and setting a few constants, we also want to make sure we sort the array as we didn&rsquo;t take care of it while collecting the data.

```typescript
var ul = document.getElementById("headerList");
var url = "https://albertovaldez5.gitlab.io/data-";
function getNum(a){
    return parseInt(a.path.split("/")[0].split("-")[1]);
}
headerData.sort((a, b) => getNum(a) - getNum(b));
```

Now we should make a main function for iterating over the data and adding all elements to the document. We want to create an `h3` element per topic and add the headers of said topic as a list. Then we also want to add the `h3` value to the Table of Contents, so we are going to need a function for that.

```typescript
function initHeaderList(){
    headerData.forEach((topic) => {
        let head = document.createElement("h3");
        head.setAttribute("id", topic.name);
        head.innerHTML = `${topic.path.split("/")[0]} - ${topic.name}`.toUpperCase();
        ul.append(head);
        addToTOC(head.innerHTML.replace("WEEK-", ""), topic.name);
        topic.data.forEach((value) => {
            let li = document.createElement("div");
            let a = document.createElement("a");
            let ref = `${url}${topic.path.replace("public/", "")}#${value.id}`;
            a.setAttribute("href", ref);
            li.append(a);
            ul.append(li);
            a.innerHTML = `${value.text}`;
            if (value.name === "h3"){
                a.setAttribute("style", "padding-left: 20px;");
            }
        })
    })
}
```

Now we can make the function for adding the header to the table of contents too.

```typescript
function addToTOC(text: string, id: string){
    let toc = document.getElementById("text-table-of-contents");
    let tocUL = toc.children[0];
    let li = document.createElement("li");
    let a = document.createElement("a");
    let href =`#${id}`;
    a.innerHTML = `${text}`;
    a.setAttribute("href", href);
    li.append(a);
    tocUL.append(li);
}
```

Here is the result of the complete script, with an added call to the main function at the end.

```typescript
var ul = document.getElementById("headerList");
var url = "https://albertovaldez5.gitlab.io/data-";
function getNum(a){
    return parseInt(a.path.split("/")[0].split("-")[1]);
}
headerData.sort((a, b) => getNum(a) - getNum(b));
function initHeaderList(){
    headerData.forEach((topic) => {
        let head = document.createElement("h3");
        head.setAttribute("id", topic.name);
        head.innerHTML = `${topic.path.split("/")[0]} - ${topic.name}`.toUpperCase();
        ul.append(head);
        addToTOC(head.innerHTML.replace("WEEK-", ""), topic.name);
        topic.data.forEach((value) => {
            let li = document.createElement("div");
            let a = document.createElement("a");
            let ref = `${url}${topic.path.replace("public/", "")}#${value.id}`;
            a.setAttribute("href", ref);
            li.append(a);
            ul.append(li);
            a.innerHTML = `${value.text}`;
            if (value.name === "h3"){
                a.setAttribute("style", "padding-left: 20px;");
            }
        })
    })
}
function addToTOC(text: string, id: string){
    let toc = document.getElementById("text-table-of-contents");
    let tocUL = toc.children[0];
    let li = document.createElement("li");
    let a = document.createElement("a");
    let href =`#${id}`;
    a.innerHTML = `${text}`;
    a.setAttribute("href", href);
    li.append(a);
    tocUL.append(li);
}
initHeaderList();
```

Done!

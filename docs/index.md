# Introduction

The table of contents on the left side helps navigate the document. Code snippets can be copied by clicking the button with the language name in the top right. Documentation on site generation [here](./metadata.md).

```python
print("Hello World")
```

    Hello World

<ul id="headerList">
</ul>
<script src="data.js"></script>
<script src="index.js"></script>

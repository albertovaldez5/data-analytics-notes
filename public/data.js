let headerData = [
{
    "name": "etl",
    "path": "week-08/public/build/etl.html",
    "data": [
        {
            "id": "etl-process",
            "text": "1. ETL process",
            "name": "h2"
        },
        {
            "id": "python-functions",
            "text": "2. Python Functions",
            "name": "h2"
        },
        {
            "id": "lambda-functions",
            "text": "3. Lambda Functions",
            "name": "h2"
        },
        {
            "id": "list-comprehensions",
            "text": "4. List Comprehensions",
            "name": "h2"
        },
        {
            "id": "double-comprehension",
            "text": "4.1. Double Comprehension",
            "name": "h3"
        },
        {
            "id": "regular-expressions",
            "text": "5. Regular Expressions",
            "name": "h2"
        },
        {
            "id": "basic-metacharacters",
            "text": "5.1. Basic Metacharacters",
            "name": "h3"
        },
        {
            "id": "quantifiers",
            "text": "5.2. Quantifiers",
            "name": "h3"
        },
        {
            "id": "sets",
            "text": "5.3. Sets",
            "name": "h3"
        },
        {
            "id": "special-sequences",
            "text": "5.4. Special Sequences",
            "name": "h3"
        },
        {
            "id": "assertion",
            "text": "5.5. Assertion",
            "name": "h3"
        },
        {
            "id": "repetition-metacharacters",
            "text": "5.6. Repetition Metacharacters",
            "name": "h3"
        },
        {
            "id": "special-groups",
            "text": "5.7. Special Groups",
            "name": "h3"
        },
        {
            "id": "regex-cases",
            "text": "6. Regex Cases",
            "name": "h2"
        },
        {
            "id": "password-validation",
            "text": "6.1. Password Validation",
            "name": "h3"
        },
        {
            "id": "time-format-validation",
            "text": "6.2. Time Format Validation",
            "name": "h3"
        },
        {
            "id": "email-validation",
            "text": "6.3. Email Validation",
            "name": "h3"
        },
        {
            "id": "username-validation",
            "text": "6.4. Username Validation",
            "name": "h3"
        },
        {
            "id": "etl-regex-cases",
            "text": "7. ETL Regex Cases",
            "name": "h2"
        },
        {
            "id": "word-extraction-and-replacement",
            "text": "7.1. Word Extraction and Replacement",
            "name": "h3"
        },
        {
            "id": "matching-a-sentence",
            "text": "7.2. Matching a sentence",
            "name": "h3"
        },
        {
            "id": "regex-with-functions",
            "text": "8. Regex with Functions",
            "name": "h2"
        },
        {
            "id": "generators-and-globs",
            "text": "9. Generators and Globs",
            "name": "h2"
        }
    ]
},
{
    "name": "apis",
    "path": "week-06/public/build/apis.html",
    "data": [
        {
            "id": "apis",
            "text": "1. APIs",
            "name": "h2"
        },
        {
            "id": "json-response",
            "text": "1.1. JSON Response",
            "name": "h3"
        },
        {
            "id": "connecting-to-apis-in-python",
            "text": "2. Connecting to APIs in Python",
            "name": "h2"
        },
        {
            "id": "preparing-the-request",
            "text": "2.1. Preparing the request",
            "name": "h3"
        },
        {
            "id": "making-the-request",
            "text": "2.2. Making the request",
            "name": "h3"
        },
        {
            "id": "getting-the-data",
            "text": "2.3. Getting the data",
            "name": "h3"
        },
        {
            "id": "request-parameters",
            "text": "2.4. Request Parameters",
            "name": "h3"
        },
        {
            "id": "working-with-response-s-data",
            "text": "3. Working with Response\u2019s Data",
            "name": "h2"
        },
        {
            "id": "adding-the-data-to-a-dataframe",
            "text": "3.1. Adding the Data to a DataFrame",
            "name": "h3"
        },
        {
            "id": "try-and-except",
            "text": "3.2. Try and Except",
            "name": "h3"
        },
        {
            "id": "script-for-making-api-calls",
            "text": "4. Script for making API calls",
            "name": "h2"
        },
        {
            "id": "many-api-calls",
            "text": "4.1. Many API calls",
            "name": "h3"
        },
        {
            "id": "storing-the-result",
            "text": "4.2. Storing the result",
            "name": "h3"
        },
        {
            "id": "api-call-generator",
            "text": "5. API Call Generator",
            "name": "h2"
        },
        {
            "id": "generator-function",
            "text": "5.1. Generator function",
            "name": "h3"
        },
        {
            "id": "storing-in-a-database",
            "text": "5.2. Storing in a database",
            "name": "h3"
        },
        {
            "id": "google-api",
            "text": "6. Google API",
            "name": "h2"
        },
        {
            "id": "geocoding-api",
            "text": "6.1. Geocoding API",
            "name": "h3"
        },
        {
            "id": "google-places",
            "text": "6.2. Google Places",
            "name": "h3"
        },
        {
            "id": "google-api-example",
            "text": "6.3. Google API Example",
            "name": "h3"
        },
        {
            "id": "gmaps-jupyter",
            "text": "7. Gmaps Jupyter",
            "name": "h2"
        },
        {
            "id": "markers",
            "text": "7.1. Markers",
            "name": "h3"
        },
        {
            "id": "heatmap",
            "text": "7.2. Heatmap",
            "name": "h3"
        }
    ]
},
{
    "name": "sql",
    "path": "week-07/public/build/sql.html",
    "data": [
        {
            "id": "introduction",
            "text": "1. Introduction",
            "name": "h2"
        },
        {
            "id": "relational-databases",
            "text": "2. Relational Databases",
            "name": "h2"
        },
        {
            "id": "database-normalization",
            "text": "2.1. Database Normalization",
            "name": "h3"
        },
        {
            "id": "entity-relationship-diagram",
            "text": "2.2. Entity Relationship Diagram",
            "name": "h3"
        },
        {
            "id": "schemas",
            "text": "2.3. Schemas",
            "name": "h3"
        },
        {
            "id": "postgresql",
            "text": "3. Postgresql",
            "name": "h2"
        },
        {
            "id": "psql-commands",
            "text": "3.1. Psql commands",
            "name": "h3"
        },
        {
            "id": "basic-queries",
            "text": "4. Basic Queries",
            "name": "h2"
        },
        {
            "id": "create-and-drop-tables",
            "text": "4.1. Create and Drop Tables",
            "name": "h3"
        },
        {
            "id": "inserting-data",
            "text": "4.2. Inserting Data",
            "name": "h3"
        },
        {
            "id": "joins",
            "text": "4.3. Joins",
            "name": "h3"
        },
        {
            "id": "loading-the-pagila-db",
            "text": "5. Loading the Pagila DB",
            "name": "h2"
        },
        {
            "id": "loading-the-schema",
            "text": "5.1. Loading the Schema",
            "name": "h3"
        },
        {
            "id": "intermediate-queries",
            "text": "6. Intermediate Queries",
            "name": "h2"
        },
        {
            "id": "aggregate-functions",
            "text": "6.1. Aggregate Functions",
            "name": "h3"
        },
        {
            "id": "where-and-having",
            "text": "6.2. Where and Having",
            "name": "h3"
        },
        {
            "id": "order-by",
            "text": "6.3. Order By",
            "name": "h3"
        },
        {
            "id": "including-joins",
            "text": "6.4. Including Joins",
            "name": "h3"
        },
        {
            "id": "distinct-on",
            "text": "6.5. Distinct On",
            "name": "h3"
        },
        {
            "id": "subqueries",
            "text": "6.6. Subqueries",
            "name": "h3"
        },
        {
            "id": "store-query-results",
            "text": "7. Store Query Results",
            "name": "h2"
        },
        {
            "id": "using-into",
            "text": "7.1. Using Into",
            "name": "h3"
        },
        {
            "id": "using-views",
            "text": "7.2. Using Views",
            "name": "h3"
        },
        {
            "id": "mastering-joins",
            "text": "8. Mastering Joins",
            "name": "h2"
        },
        {
            "id": "setup-the-database",
            "text": "8.1. Setup the Database",
            "name": "h3"
        },
        {
            "id": "inner-join",
            "text": "8.2. Inner Join",
            "name": "h3"
        },
        {
            "id": "left-join",
            "text": "8.3. Left Join",
            "name": "h3"
        },
        {
            "id": "right-join",
            "text": "8.4. Right Join",
            "name": "h3"
        },
        {
            "id": "join-and-where",
            "text": "8.5. Join and Where",
            "name": "h3"
        },
        {
            "id": "aggregation",
            "text": "8.6. Aggregation",
            "name": "h3"
        },
        {
            "id": "join-multiple-tables",
            "text": "8.7. Join Multiple Tables",
            "name": "h3"
        }
    ]
},
{
    "name": "sqlalchemy",
    "path": "week-09/public/build/sqlalchemy.html",
    "data": [
        {
            "id": "sql-alchemy",
            "text": "1. SQL Alchemy",
            "name": "h2"
        },
        {
            "id": "basics",
            "text": "1.1. Basics",
            "name": "h3"
        },
        {
            "id": "sql-queries-and-pandas",
            "text": "1.2. SQL Queries and Pandas",
            "name": "h3"
        },
        {
            "id": "object-relational-mapper-orm",
            "text": "1.3. Object Relational Mapper (ORM)",
            "name": "h3"
        },
        {
            "id": "queries-with-python-objects",
            "text": "1.4. Queries with Python objects",
            "name": "h3"
        },
        {
            "id": "inspecting-the-datadase",
            "text": "2. Inspecting the Datadase",
            "name": "h2"
        },
        {
            "id": "inspecting-the-engine",
            "text": "2.1. Inspecting the Engine",
            "name": "h3"
        },
        {
            "id": "using-metadata-and-tables",
            "text": "2.2. Using MetaData and Tables",
            "name": "h3"
        },
        {
            "id": "working-with-dates",
            "text": "3. Working with Dates",
            "name": "h2"
        },
        {
            "id": "filtering-dates-using-func",
            "text": "3.1. Filtering dates using func",
            "name": "h3"
        },
        {
            "id": "filtering-dates-using-datetime",
            "text": "3.2. Filtering dates using datetime",
            "name": "h3"
        },
        {
            "id": "boilerplate",
            "text": "4. Boilerplate",
            "name": "h2"
        },
        {
            "id": "basic-imports",
            "text": "4.1. Basic Imports",
            "name": "h3"
        },
        {
            "id": "sql-engine",
            "text": "4.2. SQL Engine",
            "name": "h3"
        },
        {
            "id": "classes-and-metadata",
            "text": "4.3. Classes and Metadata",
            "name": "h3"
        },
        {
            "id": "complex-sql-queries",
            "text": "5. Complex SQL Queries",
            "name": "h2"
        },
        {
            "id": "invoice-totals",
            "text": "5.1. Invoice Totals",
            "name": "h3"
        },
        {
            "id": "postal-codes-from-usa",
            "text": "5.2. Postal Codes from USA",
            "name": "h3"
        },
        {
            "id": "item-totals",
            "text": "5.3. Item Totals",
            "name": "h3"
        },
        {
            "id": "descending-by-total",
            "text": "5.4. Descending by Total",
            "name": "h3"
        }
    ]
},
{
    "name": "leaflet",
    "path": "week-13/public/build/leaflet.html",
    "data": [
        {
            "id": "leaflet",
            "text": "1. Leaflet",
            "name": "h2"
        },
        {
            "id": "including-leaflet",
            "text": "1.1. Including Leaflet",
            "name": "h3"
        },
        {
            "id": "implementing-leaflet",
            "text": "1.2. Implementing Leaflet",
            "name": "h3"
        },
        {
            "id": "drawing-markers",
            "text": "2. Drawing Markers",
            "name": "h2"
        },
        {
            "id": "custom-icons",
            "text": "2.1. Custom icons",
            "name": "h3"
        },
        {
            "id": "example-multiple-markers",
            "text": "2.2. Example: Multiple Markers",
            "name": "h3"
        },
        {
            "id": "drawing-shapes",
            "text": "3. Drawing Shapes",
            "name": "h2"
        },
        {
            "id": "example-drawing",
            "text": "3.1. Example: Drawing",
            "name": "h3"
        },
        {
            "id": "layer-controls",
            "text": "4. Layer Controls",
            "name": "h2"
        },
        {
            "id": "mapbox-styles",
            "text": "4.1. Mapbox Styles",
            "name": "h3"
        },
        {
            "id": "json-and-geojson",
            "text": "5. JSON and GeoJSON",
            "name": "h2"
        },
        {
            "id": "geojson-example",
            "text": "6. GeoJSON Example",
            "name": "h2"
        },
        {
            "id": "geojson-file",
            "text": "6.1. GeoJSON file",
            "name": "h3"
        },
        {
            "id": "basic-map-code",
            "text": "6.2. Basic Map Code",
            "name": "h3"
        },
        {
            "id": "using-geojson-data",
            "text": "6.3. Using GeoJSON data",
            "name": "h3"
        },
        {
            "id": "on-each-feature",
            "text": "6.4. On Each Feature",
            "name": "h3"
        },
        {
            "id": "results",
            "text": "6.5. Results",
            "name": "h3"
        },
        {
            "id": "leaflet-plugins",
            "text": "7. Leaflet Plugins",
            "name": "h2"
        }
    ]
},
{
    "name": "tableau",
    "path": "week-14/public/build/tableau.html",
    "data": [
        {
            "id": "introduction",
            "text": "1. Introduction",
            "name": "h2"
        },
        {
            "id": "the-tableau-workflow",
            "text": "2. The Tableau Workflow",
            "name": "h2"
        },
        {
            "id": "filtering-data",
            "text": "2.1. Filtering Data",
            "name": "h3"
        },
        {
            "id": "worksheets",
            "text": "2.2. Worksheets",
            "name": "h3"
        },
        {
            "id": "visualization",
            "text": "2.3. Visualization",
            "name": "h3"
        },
        {
            "id": "next-sheet",
            "text": "2.4. Next Sheet",
            "name": "h3"
        },
        {
            "id": "dates",
            "text": "2.5. Dates",
            "name": "h3"
        },
        {
            "id": "customization",
            "text": "2.6. Customization",
            "name": "h3"
        },
        {
            "id": "multiple-marks",
            "text": "2.7. Multiple Marks",
            "name": "h3"
        },
        {
            "id": "calculated-fields",
            "text": "2.8. Calculated Fields",
            "name": "h3"
        },
        {
            "id": "map-charts",
            "text": "3. Map Charts",
            "name": "h2"
        },
        {
            "id": "maps-and-location",
            "text": "3.1. Maps and Location",
            "name": "h3"
        },
        {
            "id": "using-label-values",
            "text": "3.2. Using Label Values",
            "name": "h3"
        },
        {
            "id": "map-layers",
            "text": "3.3. Map Layers",
            "name": "h3"
        },
        {
            "id": "dashboards",
            "text": "4. Dashboards",
            "name": "h2"
        },
        {
            "id": "basics",
            "text": "4.1. Basics",
            "name": "h3"
        },
        {
            "id": "dashboard-workflow",
            "text": "4.2. Dashboard Workflow",
            "name": "h3"
        },
        {
            "id": "actions-and-filters",
            "text": "4.3. Actions and Filters",
            "name": "h3"
        },
        {
            "id": "stories",
            "text": "4.4. Stories",
            "name": "h3"
        }
    ]
},
{
    "name": "rstats",
    "path": "week-15/public/build/rstats.html",
    "data": [
        {
            "id": "introduction-to-r",
            "text": "1. Introduction to R",
            "name": "h2"
        },
        {
            "id": "environment-and-packages",
            "text": "1.1. Environment and Packages",
            "name": "h3"
        },
        {
            "id": "syntax",
            "text": "2. Syntax",
            "name": "h2"
        },
        {
            "id": "variables",
            "text": "2.1. Variables",
            "name": "h3"
        },
        {
            "id": "data-structures",
            "text": "2.2. Data Structures",
            "name": "h3"
        },
        {
            "id": "functions",
            "text": "2.3. Functions",
            "name": "h3"
        },
        {
            "id": "loops-and-logic",
            "text": "2.4. Loops and Logic",
            "name": "h3"
        },
        {
            "id": "lists",
            "text": "2.5. Lists",
            "name": "h3"
        },
        {
            "id": "code-example",
            "text": "2.6. Code Example",
            "name": "h3"
        },
        {
            "id": "vectors",
            "text": "3. Vectors",
            "name": "h2"
        },
        {
            "id": "vector-operations",
            "text": "3.1. Vector Operations",
            "name": "h3"
        },
        {
            "id": "vectors-name",
            "text": "3.2. Vectors Name",
            "name": "h3"
        },
        {
            "id": "pipe-operator",
            "text": "3.3. Pipe Operator",
            "name": "h3"
        },
        {
            "id": "tibbles",
            "text": "4. Tibbles",
            "name": "h2"
        },
        {
            "id": "tibble-basics",
            "text": "4.1. Tibble Basics",
            "name": "h3"
        },
        {
            "id": "filtering-data",
            "text": "4.2. Filtering Data",
            "name": "h3"
        },
        {
            "id": "mutate-and-summary",
            "text": "4.3. Mutate and Summary",
            "name": "h3"
        },
        {
            "id": "grouping-data",
            "text": "4.4. Grouping Data",
            "name": "h3"
        },
        {
            "id": "reading-files-with-r",
            "text": "5. Reading Files with R",
            "name": "h2"
        },
        {
            "id": "working-with-diectories",
            "text": "5.1. Working with diectories",
            "name": "h3"
        },
        {
            "id": "reading-csv-files",
            "text": "5.2. Reading CSV files",
            "name": "h3"
        },
        {
            "id": "reading-json-files",
            "text": "5.3. Reading JSON files",
            "name": "h3"
        },
        {
            "id": "hypothesis-testing",
            "text": "6. Hypothesis Testing",
            "name": "h2"
        },
        {
            "id": "exploratory-data-analysis",
            "text": "6.1. Exploratory Data Analysis",
            "name": "h3"
        },
        {
            "id": "hypothesis",
            "text": "6.2. Hypothesis",
            "name": "h3"
        },
        {
            "id": "testing-the-hypothesis",
            "text": "6.3. Testing the Hypothesis",
            "name": "h3"
        },
        {
            "id": "types-of-tests",
            "text": "6.4. Types of Tests",
            "name": "h3"
        },
        {
            "id": "hypothesis-testing-in-r",
            "text": "7. Hypothesis Testing in R",
            "name": "h2"
        },
        {
            "id": "one-sample-t-test",
            "text": "7.1. One-Sample T-Test",
            "name": "h3"
        },
        {
            "id": "two-sample-t-test",
            "text": "7.2. Two-Sample T-Test",
            "name": "h3"
        },
        {
            "id": "t-test-example-in-r",
            "text": "7.3. T.Test Example in R",
            "name": "h3"
        },
        {
            "id": "anova-test",
            "text": "8. ANOVA Test",
            "name": "h2"
        },
        {
            "id": "anova-test-in-r",
            "text": "8.1. ANOVA Test in R",
            "name": "h3"
        },
        {
            "id": "linear-relationship",
            "text": "9. Linear Relationship",
            "name": "h2"
        }
    ]
},
{
    "name": "plotly",
    "path": "week-12/public/build/plotly.html",
    "data": [
        {
            "id": "plotly",
            "text": "1. Plotly",
            "name": "h2"
        },
        {
            "id": "importing-plotly",
            "text": "1.1. Importing Plotly",
            "name": "h3"
        },
        {
            "id": "using-plotly",
            "text": "1.2. Using Plotly",
            "name": "h3"
        },
        {
            "id": "filtering-data",
            "text": "2. Filtering Data",
            "name": "h2"
        },
        {
            "id": "filter-and-map",
            "text": "2.1. Filter and Map",
            "name": "h3"
        },
        {
            "id": "example-filter",
            "text": "2.2. Example: Filter",
            "name": "h3"
        },
        {
            "id": "sorting-data",
            "text": "3. Sorting Data",
            "name": "h2"
        },
        {
            "id": "sorting",
            "text": "3.1. Sorting",
            "name": "h3"
        },
        {
            "id": "slicing",
            "text": "3.2. Slicing",
            "name": "h3"
        },
        {
            "id": "example-slicing",
            "text": "3.3. Example: Slicing",
            "name": "h3"
        },
        {
            "id": "cors",
            "text": "4. CORS",
            "name": "h2"
        },
        {
            "id": "multiple-traces",
            "text": "5. Multiple Traces",
            "name": "h2"
        },
        {
            "id": "dropdown-menus",
            "text": "6. Dropdown Menus",
            "name": "h2"
        },
        {
            "id": "promises",
            "text": "7. Promises",
            "name": "h2"
        },
        {
            "id": "json-requests",
            "text": "7.1. JSON Requests",
            "name": "h3"
        }
    ]
},
{
    "name": "pandas",
    "path": "week-04/public/build/pandas.html",
    "data": [
        {
            "id": "pandas-basics",
            "text": "1. Pandas Basics",
            "name": "h2"
        },
        {
            "id": "creating-dataframes",
            "text": "1.1. Creating DataFrames",
            "name": "h3"
        },
        {
            "id": "reading-file-into-dataframe",
            "text": "1.2. Reading file into DataFrame",
            "name": "h3"
        },
        {
            "id": "describing-the-dataframe",
            "text": "1.3. Describing the DataFrame",
            "name": "h3"
        },
        {
            "id": "setting-the-index",
            "text": "1.4. Setting the Index",
            "name": "h3"
        },
        {
            "id": "renaming-a-column",
            "text": "1.5. Renaming a Column",
            "name": "h3"
        },
        {
            "id": "renaming-multiple-columns",
            "text": "1.6. Renaming Multiple Columns",
            "name": "h3"
        },
        {
            "id": "getting-types-of-values-in-columns",
            "text": "1.7. Getting Types of Values in Columns",
            "name": "h3"
        },
        {
            "id": "pandas-operations",
            "text": "2. Pandas Operations",
            "name": "h2"
        },
        {
            "id": "counting-values-in-a-dataframe",
            "text": "2.1. Counting Values in a DataFrame",
            "name": "h3"
        },
        {
            "id": "counting-unique-values-in-a-series",
            "text": "2.2. Counting Unique Values in a Series",
            "name": "h3"
        },
        {
            "id": "sorting-values",
            "text": "2.3. Sorting Values",
            "name": "h3"
        },
        {
            "id": "finding-empty-values",
            "text": "2.4. Finding Empty Values",
            "name": "h3"
        },
        {
            "id": "finding-non-empty-values",
            "text": "2.5. Finding Non-Empty Values",
            "name": "h3"
        },
        {
            "id": "deleting-rows-with-missing-data",
            "text": "2.6. Deleting rows with missing data",
            "name": "h3"
        },
        {
            "id": "adding-missing-data-to-rows",
            "text": "2.7. Adding missing data to rows",
            "name": "h3"
        },
        {
            "id": "removing-duplicates",
            "text": "2.8. Removing Duplicates",
            "name": "h3"
        },
        {
            "id": "pandas-transformations",
            "text": "3. Pandas Transformations",
            "name": "h2"
        },
        {
            "id": "converting-data-to-a-list",
            "text": "3.1. Converting data to a List",
            "name": "h3"
        },
        {
            "id": "merging-dataframes",
            "text": "3.2. Merging DataFrames",
            "name": "h3"
        },
        {
            "id": "finding-values-that-pass-a-condition",
            "text": "3.3. Finding Values that pass a Condition",
            "name": "h3"
        },
        {
            "id": "getting-the-values-that-pass-a-condition",
            "text": "3.4. Getting the Values that pass a Condition",
            "name": "h3"
        },
        {
            "id": "getting-values-with-multiple-conditions",
            "text": "3.5. Getting Values with Multiple Conditions",
            "name": "h3"
        },
        {
            "id": "applying-a-function-to-all-values-in-a-column",
            "text": "3.6. Applying a function to all Values in a Column",
            "name": "h3"
        },
        {
            "id": "grouping-values-by-different-operations",
            "text": "3.7. Grouping Values by Different Operations",
            "name": "h3"
        },
        {
            "id": "grouping-the-data-in-bins-binning",
            "text": "3.8. Grouping the Data in Bins (Binning)",
            "name": "h3"
        }
    ]
},
{
    "name": "ml",
    "path": "week-17/public/build/ml.html",
    "data": [
        {
            "id": "machine-learning",
            "text": "1. Machine Learning",
            "name": "h2"
        },
        {
            "id": "supervised-learning",
            "text": "1.1. Supervised Learning",
            "name": "h3"
        },
        {
            "id": "linear-regression",
            "text": "1.2. Linear regression",
            "name": "h3"
        },
        {
            "id": "scikit-learn",
            "text": "2. Scikit-learn",
            "name": "h2"
        },
        {
            "id": "univariate-linear-regression",
            "text": "2.1. Univariate Linear Regression",
            "name": "h3"
        },
        {
            "id": "quantifying-regression",
            "text": "2.2. Quantifying Regression",
            "name": "h3"
        },
        {
            "id": "improving-model-with-tests",
            "text": "2.3. Improving Model with Tests",
            "name": "h3"
        },
        {
            "id": "linear-regression-example",
            "text": "3. Linear Regression Example",
            "name": "h2"
        },
        {
            "id": "load-the-data",
            "text": "3.1. Load the Data",
            "name": "h3"
        },
        {
            "id": "assign-the-data-to-x-and-y",
            "text": "3.2. Assign the data to X and y",
            "name": "h3"
        },
        {
            "id": "plot-the-data",
            "text": "3.3. Plot the data",
            "name": "h3"
        },
        {
            "id": "split-learn-and-test-data",
            "text": "3.4. Split Learn and Test data",
            "name": "h3"
        },
        {
            "id": "create-and-fit-the-model",
            "text": "3.5. Create and Fit the Model",
            "name": "h3"
        },
        {
            "id": "calculate-mse-and-r2",
            "text": "3.6. Calculate MSE and R2",
            "name": "h3"
        },
        {
            "id": "calculate-r2-score",
            "text": "3.7. Calculate R2 Score",
            "name": "h3"
        },
        {
            "id": "conclusion",
            "text": "3.8. Conclusion",
            "name": "h3"
        },
        {
            "id": "logistic-regression",
            "text": "4. Logistic Regression",
            "name": "h2"
        },
        {
            "id": "logistic-regression-in-python",
            "text": "4.1. Logistic regression in python",
            "name": "h3"
        },
        {
            "id": "logistic-regression-example",
            "text": "5. Logistic Regression Example",
            "name": "h2"
        },
        {
            "id": "imports",
            "text": "5.1. Imports",
            "name": "h3"
        },
        {
            "id": "load-the-data",
            "text": "5.2. Load the data",
            "name": "h3"
        },
        {
            "id": "assign-data",
            "text": "5.3. Assign data",
            "name": "h3"
        },
        {
            "id": "init-and-train-the-data",
            "text": "5.4. Init and train the data",
            "name": "h3"
        },
        {
            "id": "make-predictions",
            "text": "5.5. Make predictions",
            "name": "h3"
        },
        {
            "id": "evaluate-model",
            "text": "5.6. Evaluate model",
            "name": "h3"
        },
        {
            "id": "storing-the-model",
            "text": "5.7. Storing the Model",
            "name": "h3"
        },
        {
            "id": "reloading-the-model",
            "text": "5.8. Reloading the Model",
            "name": "h3"
        },
        {
            "id": "confusion-matrix",
            "text": "6. Confusion Matrix",
            "name": "h2"
        },
        {
            "id": "measuring-accuracy",
            "text": "6.1. Measuring Accuracy",
            "name": "h3"
        },
        {
            "id": "confusion-matrix-in-python",
            "text": "6.2. Confusion Matrix in Python",
            "name": "h3"
        },
        {
            "id": "support-vector-machine",
            "text": "7. Support Vector Machine",
            "name": "h2"
        },
        {
            "id": "svms-in-practice",
            "text": "7.1. SVMs in practice",
            "name": "h3"
        },
        {
            "id": "decision-trees",
            "text": "8. Decision Trees",
            "name": "h2"
        },
        {
            "id": "decision-trees-in-python",
            "text": "8.1. Decision Trees in Python",
            "name": "h3"
        },
        {
            "id": "aggregation",
            "text": "8.2. Aggregation",
            "name": "h3"
        },
        {
            "id": "random-forest-by-hand",
            "text": "9. Random Forest by Hand",
            "name": "h2"
        },
        {
            "id": "org5666fc5",
            "text": "9.1. Imports",
            "name": "h3"
        },
        {
            "id": "create-data",
            "text": "9.2. Create Data",
            "name": "h3"
        },
        {
            "id": "predict",
            "text": "9.3. Predict",
            "name": "h3"
        },
        {
            "id": "bagging",
            "text": "9.4. Bagging",
            "name": "h3"
        },
        {
            "id": "random-forest-pre-built",
            "text": "9.5. Random Forest Pre-built",
            "name": "h3"
        },
        {
            "id": "ada-boost-classifier",
            "text": "9.6. Ada-Boost Classifier",
            "name": "h3"
        },
        {
            "id": "choosing-an-ensemble-model",
            "text": "10. Choosing an Ensemble Model",
            "name": "h2"
        },
        {
            "id": "org545301d",
            "text": "10.1. Imports",
            "name": "h3"
        },
        {
            "id": "loading-the-data",
            "text": "10.2. Loading the Data",
            "name": "h3"
        },
        {
            "id": "creating-a-tester-function",
            "text": "10.3. Creating a Tester function",
            "name": "h3"
        },
        {
            "id": "using-the-function-to-compare-models",
            "text": "10.4. Using the function to compare models",
            "name": "h3"
        },
        {
            "id": "feature-selection-with-random-forest",
            "text": "11. Feature Selection with Random Forest",
            "name": "h2"
        },
        {
            "id": "org44bd283",
            "text": "11.1. Imports",
            "name": "h3"
        },
        {
            "id": "load-data",
            "text": "11.2. Load Data",
            "name": "h3"
        },
        {
            "id": "split-the-data",
            "text": "11.3. Split the Data",
            "name": "h3"
        },
        {
            "id": "creating-a-first-model",
            "text": "11.4. Creating a first Model",
            "name": "h3"
        },
        {
            "id": "using-the-features-attribute",
            "text": "11.5. Using the Features attribute",
            "name": "h3"
        },
        {
            "id": "selecting-features",
            "text": "11.6. Selecting Features",
            "name": "h3"
        },
        {
            "id": "new-model-with-the-selected-features",
            "text": "11.7. New Model with the Selected Features",
            "name": "h3"
        },
        {
            "id": "evaluating-the-new-model",
            "text": "11.8. Evaluating the new Model",
            "name": "h3"
        }
    ]
},
{
    "name": "web",
    "path": "week-10/public/build/web.html",
    "data": [
        {
            "id": "html",
            "text": "1. HTML",
            "name": "h2"
        },
        {
            "id": "beautiful-soup",
            "text": "2. Beautiful Soup",
            "name": "h2"
        },
        {
            "id": "find-elements",
            "text": "3. Find Elements",
            "name": "h2"
        },
        {
            "id": "using-requests",
            "text": "4. Using Requests",
            "name": "h2"
        },
        {
            "id": "flask-templates",
            "text": "5. Flask Templates",
            "name": "h2"
        },
        {
            "id": "running-a-template",
            "text": "5.1. Running a Template",
            "name": "h3"
        },
        {
            "id": "rendering-lists",
            "text": "5.2. Rendering Lists",
            "name": "h3"
        },
        {
            "id": "scraping-app",
            "text": "6. Scraping App",
            "name": "h2"
        },
        {
            "id": "creating-a-template",
            "text": "6.1. Creating a Template",
            "name": "h3"
        },
        {
            "id": "creating-a-script",
            "text": "6.2. Creating a Script",
            "name": "h3"
        },
        {
            "id": "creating-the-flask-app",
            "text": "6.3. Creating the Flask App",
            "name": "h3"
        },
        {
            "id": "running-the-scraping-app",
            "text": "6.4. Running the Scraping App",
            "name": "h3"
        },
        {
            "id": "mongosh-commands",
            "text": "7. Mongosh Commands",
            "name": "h2"
        },
        {
            "id": "mongo-shell-commands",
            "text": "7.1. Mongo Shell commands",
            "name": "h3"
        },
        {
            "id": "mongo-in-python",
            "text": "7.2. Mongo in Python",
            "name": "h3"
        }
    ]
},
{
    "name": "deeplearning",
    "path": "week-19/public/build/deeplearning.html",
    "data": [
        {
            "id": "deep-learning",
            "text": "1. Deep Learning",
            "name": "h2"
        },
        {
            "id": "neural-networks",
            "text": "1.1. Neural Networks",
            "name": "h3"
        },
        {
            "id": "perceptrons",
            "text": "1.2. Perceptrons",
            "name": "h3"
        },
        {
            "id": "neural-networks-in-python",
            "text": "2. Neural Networks in Python",
            "name": "h2"
        },
        {
            "id": "tensorflow-and-keras",
            "text": "2.1. Tensorflow and Keras",
            "name": "h3"
        },
        {
            "id": "imports",
            "text": "2.2. Imports",
            "name": "h3"
        },
        {
            "id": "generate-data",
            "text": "2.3. Generate Data",
            "name": "h3"
        },
        {
            "id": "prepare-the-data",
            "text": "2.4. Prepare the Data",
            "name": "h3"
        },
        {
            "id": "create-and-layer-the-model",
            "text": "2.5. Create and Layer the Model",
            "name": "h3"
        },
        {
            "id": "train-the-model",
            "text": "2.6. Train the model",
            "name": "h3"
        },
        {
            "id": "visualize-the-results",
            "text": "2.7. Visualize the Results",
            "name": "h3"
        },
        {
            "id": "evaluate-the-model",
            "text": "2.8. Evaluate the Model",
            "name": "h3"
        },
        {
            "id": "re-training-the-model",
            "text": "2.9. Re-training the model",
            "name": "h3"
        },
        {
            "id": "neural-network-example-2",
            "text": "3. Neural Network Example 2",
            "name": "h2"
        },
        {
            "id": "imports",
            "text": "3.1. Imports",
            "name": "h3"
        },
        {
            "id": "load-data",
            "text": "3.2. Load Data",
            "name": "h3"
        },
        {
            "id": "create-and-layer-the-model",
            "text": "3.3. Create and Layer the Model",
            "name": "h3"
        },
        {
            "id": "compile-and-train-the-model",
            "text": "3.4. Compile and Train the Model",
            "name": "h3"
        },
        {
            "id": "evaluate-the-model",
            "text": "3.5. Evaluate the Model",
            "name": "h3"
        },
        {
            "id": "visualize-the-results",
            "text": "3.6. Visualize the Results",
            "name": "h3"
        },
        {
            "id": "org9122147",
            "text": "3.7. Re-training the Model",
            "name": "h3"
        },
        {
            "id": "encoding-data",
            "text": "4. Encoding Data",
            "name": "h2"
        },
        {
            "id": "get-frequencies",
            "text": "4.1. Get Frequencies",
            "name": "h3"
        },
        {
            "id": "one-hot-encoder",
            "text": "4.2. One Hot Encoder",
            "name": "h3"
        },
        {
            "id": "standarize-the-data",
            "text": "4.3. Standarize the Data",
            "name": "h3"
        },
        {
            "id": "neural-network-example-3",
            "text": "5. Neural Network Example 3",
            "name": "h2"
        },
        {
            "id": "load-the-data",
            "text": "5.1. Load the Data",
            "name": "h3"
        },
        {
            "id": "org13250bf",
            "text": "5.2. Prepare the data",
            "name": "h3"
        },
        {
            "id": "create-the-model",
            "text": "5.3. Create the Model",
            "name": "h3"
        },
        {
            "id": "compile-and-fit-the-model",
            "text": "5.4. Compile and Fit the Model",
            "name": "h3"
        },
        {
            "id": "org00a245b",
            "text": "5.5. Evaluate the Model",
            "name": "h3"
        },
        {
            "id": "re-create-the-model-no-free-lunch",
            "text": "5.6. Re-create the Model, No Free Lunch!",
            "name": "h3"
        },
        {
            "id": "org76f1452",
            "text": "5.7. Evaluate the Model",
            "name": "h3"
        },
        {
            "id": "addressing-overfitting-and-underfitting",
            "text": "6. Addressing Overfitting and Underfitting",
            "name": "h2"
        },
        {
            "id": "getting-rid-of-outliers",
            "text": "6.1. Getting Rid of Outliers",
            "name": "h3"
        },
        {
            "id": "work-with-hyperparameters",
            "text": "6.2. Work with Hyperparameters",
            "name": "h3"
        },
        {
            "id": "model-optimization-and-keras-tuner",
            "text": "7. Model Optimization and Keras Tuner",
            "name": "h2"
        },
        {
            "id": "org7ef7930",
            "text": "7.1. Imports",
            "name": "h3"
        },
        {
            "id": "orgadf155c",
            "text": "7.2. Prepare the Data",
            "name": "h3"
        },
        {
            "id": "visualize-the-data",
            "text": "7.3. Visualize the Data",
            "name": "h3"
        },
        {
            "id": "create-a-factory-function",
            "text": "7.4. Create a Factory Function",
            "name": "h3"
        },
        {
            "id": "use-keras-tuner",
            "text": "7.5. Use Keras Tuner",
            "name": "h3"
        },
        {
            "id": "get-the-best-model",
            "text": "7.6. Get the Best Model",
            "name": "h3"
        }
    ]
},
{
    "name": "unsupervised-ml",
    "path": "week-18/public/build/unsupervised-ml.html",
    "data": [
        {
            "id": "unsupervised-machine-learning",
            "text": "1. Unsupervised Machine Learning",
            "name": "h2"
        },
        {
            "id": "clustering",
            "text": "1.1. Clustering",
            "name": "h3"
        },
        {
            "id": "data-preparation",
            "text": "2. Data preparation",
            "name": "h2"
        },
        {
            "id": "transformation-example",
            "text": "2.1. Transformation Example",
            "name": "h3"
        },
        {
            "id": "data-transformation-example-2",
            "text": "2.2. Data Transformation Example 2",
            "name": "h3"
        },
        {
            "id": "k-means-algorithm",
            "text": "3. K-Means Algorithm",
            "name": "h2"
        },
        {
            "id": "the-elbow-curve",
            "text": "3.1. The Elbow Curve",
            "name": "h3"
        },
        {
            "id": "k-cluster-example",
            "text": "4. K-Cluster Example",
            "name": "h2"
        },
        {
            "id": "load-the-data",
            "text": "4.1. Load the Data",
            "name": "h3"
        },
        {
            "id": "convert-time-to-time-delta",
            "text": "4.2. Convert time to Time Delta",
            "name": "h3"
        },
        {
            "id": "clean-the-data",
            "text": "4.3. Clean the data",
            "name": "h3"
        },
        {
            "id": "creating-a-training-set",
            "text": "4.4. Creating a Training Set",
            "name": "h3"
        },
        {
            "id": "chose-number-of-clusters-and-predict",
            "text": "4.5. Chose Number of Clusters and Predict",
            "name": "h3"
        },
        {
            "id": "visualize-the-results",
            "text": "4.6. Visualize the Results",
            "name": "h3"
        },
        {
            "id": "organize-the-results",
            "text": "4.7. Organize the Results",
            "name": "h3"
        },
        {
            "id": "principal-component-analysis",
            "text": "5. Principal Component Analysis",
            "name": "h2"
        },
        {
            "id": "example-of-pca",
            "text": "6. Example of PCA",
            "name": "h2"
        },
        {
            "id": "load-the-data",
            "text": "6.1. Load the Data",
            "name": "h3"
        },
        {
            "id": "standarize-scale-the-data",
            "text": "6.2. Standarize / Scale the data",
            "name": "h3"
        },
        {
            "id": "use-the-pca",
            "text": "6.3. Use the PCA",
            "name": "h3"
        },
        {
            "id": "k-means-clustering",
            "text": "6.4. K-means clustering",
            "name": "h3"
        },
        {
            "id": "visualize-the-results",
            "text": "6.5. Visualize the Results",
            "name": "h3"
        },
        {
            "id": "pca-example-2",
            "text": "7. PCA Example 2",
            "name": "h2"
        },
        {
            "id": "clean-the-data",
            "text": "7.1. Clean the data",
            "name": "h3"
        },
        {
            "id": "applying-pca",
            "text": "7.2. Applying PCA",
            "name": "h3"
        },
        {
            "id": "visualize-pca",
            "text": "7.3. Visualize PCA",
            "name": "h3"
        },
        {
            "id": "visualize-elbow-curve",
            "text": "7.4. Visualize Elbow Curve",
            "name": "h3"
        },
        {
            "id": "select-amount-of-clusters-and-build-model",
            "text": "7.5. Select Amount of Clusters and build Model",
            "name": "h3"
        },
        {
            "id": "visualize-the-results",
            "text": "7.6. Visualize the Results",
            "name": "h3"
        },
        {
            "id": "display-summary-statistics",
            "text": "7.7. Display summary statistics",
            "name": "h3"
        },
        {
            "id": "hierarchical-clustering",
            "text": "8. Hierarchical Clustering",
            "name": "h2"
        },
        {
            "id": "dendograms",
            "text": "8.1. Dendograms",
            "name": "h3"
        },
        {
            "id": "hierarchical-cluster-example",
            "text": "9. Hierarchical Cluster Example",
            "name": "h2"
        },
        {
            "id": "imports",
            "text": "9.1. Imports",
            "name": "h3"
        },
        {
            "id": "load-and-normalize-the-data",
            "text": "9.2. Load and Normalize the Data",
            "name": "h3"
        },
        {
            "id": "create-the-dendogram",
            "text": "9.3. Create the Dendogram",
            "name": "h3"
        },
        {
            "id": "remap-observations-into-clusters",
            "text": "9.4. Remap Observations into Clusters",
            "name": "h3"
        },
        {
            "id": "run-the-dendogram-again",
            "text": "9.5. Run the Dendogram Again",
            "name": "h3"
        }
    ]
},
{
    "name": "javascript",
    "path": "week-11/public/build/javascript.html",
    "data": [
        {
            "id": "introduction",
            "text": "1. Introduction",
            "name": "h2"
        },
        {
            "id": "running-javascript",
            "text": "2. Running Javascript",
            "name": "h2"
        },
        {
            "id": "running-from-html",
            "text": "2.1. Running from HTML",
            "name": "h3"
        },
        {
            "id": "running-from-a-file",
            "text": "2.2. Running from a file",
            "name": "h3"
        },
        {
            "id": "syntax",
            "text": "3. Syntax",
            "name": "h2"
        },
        {
            "id": "defining-variables",
            "text": "3.1. Defining Variables",
            "name": "h3"
        },
        {
            "id": "operations",
            "text": "3.2. Operations",
            "name": "h3"
        },
        {
            "id": "strings",
            "text": "3.3. Strings",
            "name": "h3"
        },
        {
            "id": "templates",
            "text": "3.4. Templates",
            "name": "h3"
        },
        {
            "id": "control-flow",
            "text": "4. Control Flow",
            "name": "h2"
        },
        {
            "id": "if-statements",
            "text": "4.1. If Statements",
            "name": "h3"
        },
        {
            "id": "comparison-operators",
            "text": "4.2. Comparison Operators",
            "name": "h3"
        },
        {
            "id": "data-structures",
            "text": "5. Data Structures",
            "name": "h2"
        },
        {
            "id": "arrays",
            "text": "5.1. Arrays",
            "name": "h3"
        },
        {
            "id": "objects",
            "text": "5.2. Objects",
            "name": "h3"
        },
        {
            "id": "example-array",
            "text": "5.3. Example: Array",
            "name": "h3"
        },
        {
            "id": "loops",
            "text": "6. Loops",
            "name": "h2"
        },
        {
            "id": "example-loops",
            "text": "6.1. Example: Loops",
            "name": "h3"
        },
        {
            "id": "functions",
            "text": "7. Functions",
            "name": "h2"
        },
        {
            "id": "built-in-functions",
            "text": "7.1. Built-in Functions",
            "name": "h3"
        },
        {
            "id": "example-functions",
            "text": "7.2. Example: Functions",
            "name": "h3"
        },
        {
            "id": "functional-javascript",
            "text": "8. Functional Javascript",
            "name": "h2"
        },
        {
            "id": "for-each",
            "text": "8.1. For Each",
            "name": "h3"
        },
        {
            "id": "the-arrow-function",
            "text": "8.2. The Arrow Function",
            "name": "h3"
        },
        {
            "id": "json",
            "text": "9. JSON",
            "name": "h2"
        },
        {
            "id": "json-and-functions",
            "text": "9.1. JSON and Functions",
            "name": "h3"
        },
        {
            "id": "example-objects",
            "text": "9.2. Example: Objects",
            "name": "h3"
        },
        {
            "id": "css",
            "text": "10. CSS",
            "name": "h2"
        },
        {
            "id": "bootstrap",
            "text": "10.1. Bootstrap",
            "name": "h3"
        },
        {
            "id": "d3",
            "text": "11. D3",
            "name": "h2"
        },
        {
            "id": "example-d3",
            "text": "11.1. Example: D3",
            "name": "h3"
        },
        {
            "id": "python-test",
            "text": "11.2. Python Test",
            "name": "h3"
        },
        {
            "id": "events",
            "text": "12. Events",
            "name": "h2"
        },
        {
            "id": "d3-event-listeners",
            "text": "12.1. D3 Event Listeners",
            "name": "h3"
        },
        {
            "id": "adding-html",
            "text": "12.2. Adding HTML",
            "name": "h3"
        },
        {
            "id": "example-events",
            "text": "12.3. Example: Events",
            "name": "h3"
        }
    ]
},
{
    "name": "bigdata",
    "path": "week-16/public/build/bigdata.html",
    "data": [
        {
            "id": "big-data",
            "text": "1. Big Data",
            "name": "h2"
        },
        {
            "id": "big-data-technologies",
            "text": "1.1. Big Data Technologies",
            "name": "h3"
        },
        {
            "id": "mrjob-library",
            "text": "1.2. MRJob library",
            "name": "h3"
        },
        {
            "id": "hadoop",
            "text": "1.3. Hadoop",
            "name": "h3"
        },
        {
            "id": "spark",
            "text": "2. Spark",
            "name": "h2"
        },
        {
            "id": "start-spark",
            "text": "2.1. Start Spark",
            "name": "h3"
        },
        {
            "id": "functions",
            "text": "2.2. Functions",
            "name": "h3"
        },
        {
            "id": "filtering",
            "text": "2.3. Filtering",
            "name": "h3"
        },
        {
            "id": "spark-schemas",
            "text": "3. Spark Schemas",
            "name": "h2"
        },
        {
            "id": "spark-example-1",
            "text": "4. Spark Example 1",
            "name": "h2"
        },
        {
            "id": "spark-example-2",
            "text": "5. Spark Example 2",
            "name": "h2"
        },
        {
            "id": "working-with-dates",
            "text": "6. Working with Dates",
            "name": "h2"
        },
        {
            "id": "plotting-data",
            "text": "7. Plotting Data",
            "name": "h2"
        }
    ]
},

]
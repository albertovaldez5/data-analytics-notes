var ul = document.getElementById("headerList");
var url = "https://albertovaldez5.gitlab.io/data-";
function getNum(a) {
    return parseInt(a.path.split("/")[0].split("-")[1]);
}
headerData.sort(function (a, b) { return getNum(a) - getNum(b); });
function initHeaderList() {
    headerData.forEach(function (topic) {
        var head = document.createElement("h3");
        head.setAttribute("id", topic.name);
        head.innerHTML = "".concat(topic.path.split("/")[0], " - ").concat(topic.name).toUpperCase();
        ul.append(head);
        addToTOC(head.innerHTML.replace("WEEK-", ""), topic.name);
        topic.data.forEach(function (value) {
            var li = document.createElement("div");
            var a = document.createElement("a");
            var ref = "".concat(url).concat(topic.path.replace("public/", ""), "#").concat(value.id);
            a.setAttribute("href", ref);
            li.append(a);
            ul.append(li);
            a.innerHTML = "".concat(value.text);
            if (value.name === "h3") {
                a.setAttribute("style", "padding-left: 20px;");
            }
        });
    });
}
function addToTOC(text, id) {
    var toc = document.getElementById("text-table-of-contents");
    var tocUL = toc.children[0];
    var li = document.createElement("li");
    var a = document.createElement("a");
    var href = "#".concat(id);
    a.innerHTML = "".concat(text);
    a.setAttribute("href", href);
    li.append(a);
    tocUL.append(li);
}
initHeaderList();

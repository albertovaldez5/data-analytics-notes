# [[file:metadata.org::#final-script][metadata]]
from dataclasses import dataclass
import json
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver import Chrome
from bs4 import BeautifulSoup as Soup
from argparse import ArgumentParser, Namespace
from typing import AsyncGenerator, Callable, Generator
from pathlib import Path
import asyncio
import re

BANS = ["scraper", "quizgame"] # banned filenames

@dataclass
class Metadata:
    """Holds data to be scraped from an html file."""

    name: str
    path: Path
    html: Soup

    def scrape(self, *args: Callable[[Soup], Generator]) -> dict:
        """Apply all given scraping functions to the metadata's html attribute.

        Returns:
            (dict): {name, path, data}
        """
        return {
            "name": f"{self.name}",
            "path": f"{self.path}",
            "data": [data for func in args for data in func(self.html)],
        }

    def __repr__(self) -> str:
        return (
            f"-----Metadata-----\n"
            f"name: {self.name}\n"
            f"path: {self.path}\n"
            f"------------------"
        )

async def main(argv: Namespace):
    """Execute all scraping tasks and store results in files.

    Args:
        argv (Namespace): -h

    Raises:
        ValueError: If input directory doesn't exist.
    """
    input_path = Path(argv.input)
    output_path = Path(argv.output)
    if not input_path.is_dir():
        raise ValueError(f"Directory {input_path} doesn't exist.")
    if not output_path.is_dir():
        Path.mkdir(output_path)
    # Scrape
    write_to_js(output_path, "let headerData = [\n", "w")
    async for metadata in get_html(get_browser(), input_path, "**/build/*.html"):
        print(metadata)
        result = json.dumps(metadata.scrape(get_headers), indent=4)
        write_to_js(output_path, f"{result},\n", "a")
    write_to_js(output_path, "\n]", "a")

async def get_html(browser: Chrome, path: Path, glob: str) -> AsyncGenerator[Metadata, None]:
    """Visit all html files in given path and get their name and html content.

    Args:
        browser (Chrome): Driver to use.
        path (Path): Path where all html files are located.
        glob: (str): Complete glob pattern to use.

    Yields:
        AsyncGenerator[Metadata, None, None]: Metadata object.
    """
    for file in path.resolve().glob(glob):
        browser.get(f"file://{path / file}")
        if browser.page_source is not None and file.stem not in BANS:
            filepath = file.relative_to(file.parent.parent.parent.parent)
            yield Metadata(file.stem, filepath, Soup(browser.page_source, "html.parser"))
    browser.quit()


def get_headers(html: Soup) -> Generator[dict, None, None]:
    """Find all h2 and h3 headers in given Soup.

    Args:
        html (Soup): parsed html.

    Yields:
        Generator[dict, None, None]: dictionary of id, text.
    """
    for h in html.find_all(re.compile('^h[2-3]$')):
        if h.get("id") is not None:
            yield {
                "id": h.get("id"),
                "text": h.text,
                "name": h.name
            }

def get_browser() -> Chrome:
    """Start a headless Chrome browser."""
    options = Options()
    options.headless = True
    service = Service(ChromeDriverManager().install())
    return Chrome(service=service, options=options)


def write_to_js(path: Path, data: str, mode: str, name: str = "data.js"):
    """Write string to data.js in given path."""
    with open(path / name, mode) as file:
        file.write(data)

if __name__ == "__main__":
    default_input = "./resources"
    default_output = "./public"
    args = ArgumentParser(
        prog="Metadata scraper",
        usage=f"python config/metadata.py -i {default_input} -o {default_output}",
        description="Scrape id, text metadata from HTML files.",
    )
    args.add_argument(
        "-i",
        "--input",
        metavar="path",
        default=f"{default_input}",
        help=f"Directory with the files to scrape. Defaults to {default_input}.",
    )
    args.add_argument(
        "-o",
        "--output",
        metavar="path",
        default=f"{default_output}",
        help=f"Directory for files output. Defaults to {default_output}",
    )
    asyncio.run(main(args.parse_args()))
# metadata ends here
